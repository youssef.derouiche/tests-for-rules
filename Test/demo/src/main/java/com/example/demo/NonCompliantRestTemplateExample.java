package com.example.demo;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.web.client.RestTemplate;

public class NonCompliantRestTemplateExample {
    public RestTemplate myRestTemplate() {
        // Noncompliant: Using simple HTTP client without SSL/TLS
        // @SuppressWarnings("unused")
        CloseableHttpClient httpClient = HttpClients.createDefault();
        return (null);
    }
}
