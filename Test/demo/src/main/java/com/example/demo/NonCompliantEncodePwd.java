package com.example.demo;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NonCompliantEncodePwd {

    @PostMapping("/updatePassword")
    public void updatePassword(String username, String newPassword) {
        // Noncompliant: Password is not encoded before being processed
        // Code to update password without encoding
        System.out.println("Password for user " + username + " updated to: " + newPassword);
    }
}
