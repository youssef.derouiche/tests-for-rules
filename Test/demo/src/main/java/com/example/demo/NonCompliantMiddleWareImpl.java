package com.example.demo;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NonCompliantMiddleWareImpl {

    private static final Logger log = LoggerFactory.getLogger(NonCompliantMiddleWareImpl.class);

    // Noncompliant: Using a hardcoded map for user authentication
    private static final Map<String, String> users = new HashMap<>();

    static {
        users.put("user1", "password1");
        users.put("user2", "password2");
    }

    @Transactional
    public boolean authenticateUser(String username, String password) {
        log.debug("Request to login with {}", username);
        // Noncompliant: Insecure authentication method without LDAP
        return users.containsKey(username) && users.get(username).equals(password);
    }
}
