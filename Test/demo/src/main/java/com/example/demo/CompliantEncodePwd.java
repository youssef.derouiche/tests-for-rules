package com.example.demo;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompliantEncodePwd {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @PostMapping("/updatePassword")
    public void updatePassword(String username, String newPassword) {
        String encodedPassword = passwordEncoder.encode(newPassword); // Compliant: Password is encoded
        // Code to update password with encoded password
        System.out.println("Password for user " + username + " updated to: " + encodedPassword);
    }
}
