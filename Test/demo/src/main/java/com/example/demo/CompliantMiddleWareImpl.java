package com.example.demo;

import java.util.Properties;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompliantMiddleWareImpl {

    private static final Logger log = LoggerFactory.getLogger(CompliantMiddleWareImpl.class);

    @Value("${java.naming.factory.initial}")
    String ldapCtxFactory;

    @Value("${java.naming.security.authentication}")
    String ldapAuthentication;

    @Value("${java.naming.referral}")
    String ldapReferral;

    @Value("${java.naming.provider.url}")
    String ldapUrl;

    @Value("${java.naming.security.principal}")
    String ldapPrincipal;

    @Transactional
    public boolean authenticateUser(String username, String password) {
        log.debug("Request to login to AD with {}", username);
        return checkLogin(username, password);
    }

    private boolean checkLogin(String username, String password) {
        Properties properties = new Properties();
        properties.put("java.naming.factory.initial", ldapCtxFactory);
        properties.put("java.naming.security.authentication", ldapAuthentication);
        properties.put("java.naming.referral", ldapReferral);
        properties.put("java.naming.provider.url", ldapUrl);
        properties.put("java.naming.security.principal", ldapPrincipal + username);
        properties.put("java.naming.security.credentials", password);

        try {
            DirContext ctx = new InitialDirContext(properties);
            SearchControls ctls = new SearchControls(2, 0L, 0, new String[] { "sAMAccountName", "displayName", "mail" },
                    false, false);
            NamingEnumeration<SearchResult> answerSearch = ctx.search(
                    "dc=bank-sud,dc=tn",
                    "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" + username + "))",
                    ctls);

            if (answerSearch.hasMore()) {
                SearchResult result = answerSearch.next();
                log.info("User authenticated: {}", result.getAttributes());
                return true;
            } else {
                log.info("User not found: {}", username);
                return false;
            }
        } catch (NamingException e) {
            log.error("LDAP authentication failed: {}", e.getMessage());
            return false;
        }
    }
}
