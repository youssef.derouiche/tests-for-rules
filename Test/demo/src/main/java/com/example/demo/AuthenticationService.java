package com.example.demo;

public class AuthenticationService {

    private int failedAttempts = 0;

    public boolean login(String username, String password) {
        if (authenticate(username, password)) {
            resetFailedAttempts();
            return true;
        } else {
            recordFailedAttempt();
            return false;
        }
    }

    private boolean authenticate(String username, String password) {
        // Simulate authentication logic
        return "admin".equals(username) && "password".equals(password);
    }

    private void resetFailedAttempts() {
        failedAttempts = 0;
    }

    private void recordFailedAttempt() {
        failedAttempts++;
        // Check if the failed attempts exceed the limit
        if (failedAttempts >= 6) {
            // Simulate a lockout mechanism
            System.out.println("Account locked due to too many failed login attempts.");
            // Lockout mechanism missing the duration control
        }
    }
}
